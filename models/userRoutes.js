const express = require ("express");
const router = express.Router();
const userController = require ("../controllers/userControllers.js");
//Route for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {

		userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));


	});

// router for user registration
router.post ("/register", (req, res) => {

	userController.registeredUser(req.body).then(resultFromController => res.send(resultFromController))

});
module.exports = router;

// Route for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

module.exports = router;

