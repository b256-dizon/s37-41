// Set up dependencies
const mongoose = require("mongoose");

// Create the schema for our courses
const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		// requires the data for this field/property to be included when creating a record
		// the "true" value defines if the field is required or not, and the second element in the array is the message we will display in the console when the data we need is not present
		required:[true, "Course Name is required"]
	};

	description {
		type:String,
		required:[true, "Coure Description is required"]

	};

	price: {
		type:Number,
		required:[true, "Price is required"]
	};

	isActive: {
		type:Boolean,
		default: true

	},
	createdOn: {
		type:Date,
		// The"new Date()" is an expression that instantiates a new "date" that stores the current date and time whenever a course is created in our database
		default: new Date()
	},

	enrolless: [
		{
			userId: {
				type:String,
				required:[true, "user ID is required"]
			},
	enrolledOn: {
		type:Date,
		default: new Date()
	},

		}

	]

})

// model -> Course
// collections -> courses
// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files
module.exports = mongoose.model("Course", courseSchema);

