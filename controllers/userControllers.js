//  contains all the business logic and functions of our application

const User = require("../models/User.js");


/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkIfEmailExists = (requestBody) => {

		User.find({email:requestBody.email}).then (result => {

			if(result.length > 0) {
return true;


			} else {

				return false;
			}
		})


}

// User Registration
/*
	BUsiness Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registeredUser =(requestBody) => {


	let newUser = new User({

		firstName: requestBody.firstName,
		lastName:requestBody.lastName,
		email: requestBody.email,
		mobileNo:requestBody.mobileNo,
		password: requestBody.password

	})


	return newUser.save().then ((user, err) => {

		if (err) {
			return false;
		} else {

			return true;
		}
	})


	}

	// User authentication
/*
	Business Logic:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.authenticateUser = (requestBody) => {

	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
	return User.findOne({email: requestBody.email}).then(result => {

		// User does not exist
		if(result == null) {

			return false;

		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
				//example. isSingle, isDone, isAdmin, areDone, etc..
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {

				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return {access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {

				return false;
			};
		};
	});
};


module.exports.registeredUser = (paramsId) => {
	return User.findById(paramsId).then((myUser, err) => {
		if(err) {
			console.log(err)
			return 'User ID';
		} else {
			return User;
		}
	})
}
